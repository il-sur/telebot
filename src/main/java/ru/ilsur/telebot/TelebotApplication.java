package ru.ilsur.telebot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.telegram.telegrambots.ApiContextInitializer;
import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.exceptions.TelegramApiRequestException;

@SpringBootApplication
public class TelebotApplication {

    public static void main(String[] args) {
        //SpringApplication.run(TelebotApplication.class, args);
        ApiContextInitializer.init(); //инициализируем апи телеграма
        TelegramBotsApi telegram = new TelegramBotsApi(); //обращаемся к апи

        Bot bot = new Bot();
        try{
            telegram.registerBot(bot);
        }catch(TelegramApiRequestException e){
            e.printStackTrace();
        }
    }

}
