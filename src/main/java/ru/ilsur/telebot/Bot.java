package ru.ilsur.telebot;

import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.send.SendPhoto;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import ru.ilsur.telebot.entity.Book;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;


public class Bot extends TelegramLongPollingBot {
    Book book = new Book();
    private long chat_id;

    /**
     * Метод для приема сообщений.
     * @param update Содержит сообщение от пользователя.
     */
    @Override
    public void onUpdateReceived(Update update) {
        update.getUpdateId();
        //класс для отправки сообщений, setChatid - выставляет id человека который написал боту
        SendMessage sendMessage = new SendMessage().setChatId(update.getMessage().getChatId()); //id того же человека
        chat_id = update.getMessage().getChatId();
        sendMessage.setText(input(update.getMessage().getText()));

            try{
                execute(sendMessage);
            }catch (TelegramApiException e){
                e.printStackTrace();
            }
            //update - пользователь, execute - отправка сообщения
    }

    /**
     * Метод возвращает имя бота, указанное при регистрации.
     * @return имя бота
     */
    @Override
    public String getBotUsername() {
        return "@ping44_bot";
    }

    /**
     * Метод возвращает token бота для связи с сервером Telegram
     * @return token для бота
     */
    @Override
    public String getBotToken() {
        return "817810923:AAExlU8K86iFYznG7KoZYy0SDr6woWcU1no";
    }

    public String input(String msg){
        if (msg.contains("Hi") || msg.contains("Hello") || msg.contains("Привет")) {
            return "Привет друг!";
        }
        if(msg.contains("Информация о книге")){
            return getInfoBook();
        }
        return msg;
    }

    public String getInfoBook(){
        SendPhoto sendPhoto = new SendPhoto();

        try(InputStream in = new URL(book.getImage()).openStream()){
            Files.copy(in, Paths.get("C:\\srgBook")); //ВЫгружает изображение с интернета
            sendPhoto.setChatId(chat_id);
            sendPhoto.setPhoto(new File("C:\\srgBook"));
            execute(sendPhoto); //отправка картинки
            Files.delete(Paths.get("C:\\srgBook")); //удаление картинки
        }catch (IOException ex){
            System.out.println("File not found");
        }catch (TelegramApiException e){
            e.printStackTrace();
        }

        String info = book.getTitle()
                + "\nАвтор" + book.getAutorName()
                + "\nЖанр" + book.getGenres()
                + "\n\nОписание\n" + book.getDescription()
                + "\n\nКоличество лайков " + book.getLikes()
                + "\n\nПоследние комментарии\n" + book.getCommentList();

        return info;
    }
}
